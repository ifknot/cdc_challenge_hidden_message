# Clinical Developers' Club #

[Facebook Page: Clinical Developers' Club](https://www.facebook.com/ClinicalDevelopers/ "Facebook Page: Clinical Developers' Club")

Stu Maitland uploaded a file in the group: Clinical Developers Club.
August 8 at 12:16pm 

### 8th August 2017 Challenge ###

Time for this weeks challenge!
You are working with a team of genetics researchers. You're told the exciting and confusing news that there may be a message from God hidden in our DNA! Through science and prayer your team has been able to work out the following:
* The message is hidden somewhere in the attached sequence of ~120,000 base pairs
* God writes in stochastic binary, with 0 represented by either A or C, and 1 represented by either T or G
* God encodes in ASCII
Can you find the message?
Prizes for fastest correct answer, and most elegant solution!

### Some clues... ###

* "...principles of dna: message is an extron amongst intron data, and frame shift may be present"
* "...there are some untranscribed regulatory regions"
* "Could be either, to make an ascii 'amino acid' of 8 chars. You'll just have to try each of the 8 possible frames!"
* " Does god use much punctuation or any numerals?"
* "Nope, the occassional comma"

### Usage... ###

**`findquote <bases input file> <messages output file>` **

### Example... ###

`findquote id100228968.seq msg`

### Hidden message... ###

>   Exit Servant
>	Is this a dagger which I see before me,
>	The handle toward my hand Come, let me clutch thee.    
>	I have thee not, and yet I see thee still.    
>	Art thou not, fatal vision, sensible    
>	To feeling as to sight or art thou but    
>	A dagger of the mind, a false creation,    
>	Proceeding from the heatoppressed brain    
>	I see thee yet, in form as palpable    
>	As this which now I draw.    
>	Thou marshallst me the way that I was going    
>	And such an instrument I was to use.    
>	Mine eyes are made the fools o the other senses,    
>	Or else worth all the rest I see thee still,    
>	And on thy blade and dudgeon gouts of blood,    
>	Which was not so before. Theres no such thing    
>	It is the bloody business which informs    
>	Thus to mine eyes. Now oer the one halfworld    
>	Nature seems dead, and wicked dreams abuse    
>	The curtaind sleep witchcraft celebrates    
>	Pale Hecates offerings, and witherd murder,    
>	Alarumd by his sentinel, the wolf,    
>	Whose howls his watch, thus with his stealthy pace.    
>	With Tarquins ravishing strides, towards his design    
>	Moves like a ghost. Thou sure and firmset earth,    
>	Hear not my steps, which way they walk, for fear    
>	Thy very stones prate of my whereabout,    
>	And take the present horror from the time,    
>	Which now suits with it. Whiles I threat, he lives    
>	Words to the heat of deeds too cold breath gives.

