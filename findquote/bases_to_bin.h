#ifndef BASES_TO_BIN_H
#define BASES_TO_BIN_H

#include <fstream>

/**
 * @brief bases_to_bin convert text file of bases (A,C,T,G) into a string of '0' and '1'
 * According to the challenge: "God writes in stochastic binary, with 0 represented by either A or C, and 1 represented by either T or G"
 * (Which isn't exactly stochastic)
 * This function expects only characters 'A', 'C', 'T' or 'G' on its input stream to do otherwise is to reap undefined behavior!
 * Methodology: Uses a switch statement with deliberate "fall through" - this approach can be considered poor programming on the grounds that it is  prone and, indeed, Swift does not allow it without the explicit 'fallthrough' keyword.
 * Personally when it used as below I think that it is intuitive but if you have a case label followed by code that falls through to another case label, I'd pretty much always consider that 'evil'.
 * @note N.B. the definition of 'evil' can be found at the ISO C++ wiki here: https://isocpp.org/wiki/faq/big-picture#defn-evil
 * @param fin - reference to the input file of bases
 * @return std::string - converted stream consisting of only '0' and/or '1' characters @note may be empty
 */
std::string bases_to_bin(std::ifstream& fin) {
    char ch;
    std::string noughts_ones;
    while(!fin.eof()) {
        fin >> ch;
        switch (ch) {
        case 'A':
        case 'C':
             noughts_ones += '0';
            break;
        case 'T':
        case 'G':
            noughts_ones += '1';
            break;
        }
    }
    return noughts_ones;
}

#endif // BASES_TO_BIN_H
