#ifndef WORD_COUNT_H
#define WORD_COUNT_H

#include <string>
#include <vector>

/**
 * @brief word_count count the number of words found from the dictionary of search words
 * Methodology: Use the string type find(std::string) member function to search the proposed quote against the dictionary of common words of 3 or more letters.
 * @note there is little or no point searching for words less thhan 3 letters long due to the high probability of 2 and definately 1 letter words appearing in a random soup of letters.
 * @param message - the candidate message
 * @param words - the search dictionary
 * @return in - the number of hits against the dictionary words
 */
int word_count(std::string message, std::vector<std::string> words) {
    if(message.length() < 3) return 0;
    int n = 0;
    //for the unitiated npos is a static member constant value with the greatest possible value for an element of type size_t - in other words it means you've reached the end :)
    for(auto word : words) if (message.find(word) != std::string::npos) n++;
    return n;
}

#endif // WORD_COUNT_H
