#include <assert.h>
#include <iostream>

#include "bases_to_bin.h"
#include "search_codons.h"
#include "word_count.h"

int main(int argc, char *argv[]) {

    assert(argc = 3);

    std::string path_in = argv[1]; // input filename
    std::string path_out = argv[2]; // output files core name
    const std::string path_words = "english_words.txt"; //could be any language

    std::cout << "Find Quote..." << std::endl;

    //read the common word language file and setup the dictionary vector
    std::ifstream fwords(path_words);
    if(!fwords.eof() && fwords.fail()) {
        std::cout << ":( file words error" << std::endl;
        exit(0);
    }
    std::vector<std::string>words;
    std::string word;
    while(getline(fwords, word)) words.push_back(word);

    //open input file and do some error checking
    std::ifstream fin(path_in);
    if(!fin.eof() && fin.fail()) {
        std::cout << ":( file in error" << std::endl;
        exit(0);
    }

    //convert input bases to binary nucloetides
    std::string bits = bases_to_bin(fin);

    //step through each frame transcribing readable characters and then searching for occurences of most common 3 letter or more words and write out to a file if found words passes a cutoff
    int cutoff = 5; //arbitrary
    for(int i = 0; i < 8; ++i) {
        std::cout << "searching offset " << i;

        std::string quote = search_codons(i, bits);
        if (word_count(quote, words) > cutoff) {
            std::ofstream fout(path_out + std::to_string(i) + ".txt");
            fout << quote;
            std::cout << " found!";
        }

        std::cout << std::endl;
    }

    std::cout << "...done!" << std::endl;
}
