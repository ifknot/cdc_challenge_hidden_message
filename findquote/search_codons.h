#ifndef SEARCH_CODONS_H
#define SEARCH_CODONS_H

#include <bitset>
#include <string>

/**
 * @brief search_codons within a frame offset match 8 bit 'codons' with readable ASCII characters
 * As per clues from challenge author, god does not use numerals nor much punctuation.
 * Retrospectively added transcription switch based on identified unique regulatory regions present in the output.
 * Methodology: The 'secret sauce' is to use the C++ bitset template type which is a hugely useful highly optimized array of bool elements that can be of any size and is has a rich behaviour.
 * Barely scratching the surface of its abilities a bitset of size 8 is used to mimick a byte by using the initialization from string constructor that takes a string of 0, 1 chars.
 * Then using the to_ulong() public member function to convert to unsigned long integer and this ulong int is then cast to a char which is then filtered for valid codon characters as per the clue.
 * @param frame - int, offset into the 'nucleotide' string to start matching
 * @param noughts_ones - string, the 'nucleotides' to search and match
 * @return string - transcribed sequence of valid characters
 */
std::string search_codons(int frame, std::string noughts_ones) {

    //The following 'regulatory' regions showed up after examing the 'codons' either side of the quote
    const int START1 = 53;
    const int START2 = 160;
    const int STOP1 = 10;
    const int STOP2 = 193;

    bool transcribe = false; //switch transcription on and off
    std::string message;

    //loop through the offset nucleotides in 8 nucleotide chunks
    for(int i = frame; i < noughts_ones.length() - 8; i += 8) {

        //string constructor initialize a bitset with an 8 nucleotide chunk using the sub string member function of the string type
        std::bitset<8>byte1(noughts_ones.substr(i, 8));

        //check for start region
        if((byte1.to_ulong() == START1) & (i + 8 < noughts_ones.length())) {
            //found first start regulatory codon search for second
            std::bitset<8>byte2(noughts_ones.substr(i + 8, 8));
            if (byte2.to_ulong() == START2) {
                //second start codon found switch on transcription
                transcribe = true;
            }
        }

        //check for stop region
        if((byte1.to_ulong() == STOP1) & (i + 8 < noughts_ones.length())) {
            std::bitset<8>byte2(noughts_ones.substr(i + 8, 8));
            if (byte2.to_ulong() == STOP2) {
                transcribe = false; //stop regulatory region found swithc off transcription
            }
        }

        //in transcribable region match only readable codons
        if(transcribe) {
            char ascii = static_cast<char>(byte1.to_ulong()); //I feel so type cast!
            if (((ascii >= 'A') & (ascii <= 'Z')) | ((ascii >= 'a') & (ascii <= 'z')) | (ascii == ' ') | (ascii == ',') | (ascii == '.')) {
                message += static_cast<char>(ascii);
            }
        }
    }

    return message;

}


#endif // SEARCH_CODONS_H
